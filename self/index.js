require('./helpers')
figlet.defaults({fontPath: "assets/fonts"});
localStorage.debug = 'völkrom'

window.Ø = {
	database: {
		lexicon: require('./database/lexicon.ndtl').self,
		horaire: require('./database/horaire.tbtl').self,		
	},

	debug: create_debug('völkrom'),
	bus: bus('völkrom'),
	router: router('/404'),
	node: document.getElementById('volkrom'),

	init() {
		this.content_node = html`<div></div>`
		this.node.appendChild(this.content_node)
		addEventListener('hashchange', () => { this.bus.emit('navigate') })

		this.router.on('/', () => {
			this.render(this.database.lexicon.home)
		})

		this.router.on('/404', () => {
			this.render(this.database.lexicon['404'])
		})

		this.router.on('/:slug', ({ slug }) => {
			this.render(this.database.lexicon[$slugify(slug)])
		})

		this.bus.on('navigate', () => {
			this.debug('navigate to '+hash())
			this.router(hash())
		})
	},

	start() {
		this.debug('starting völkrom')
		this.init()
		this.bus.emit('navigate')
	},

	render(entry) {
		let self = new Runic(entry.long, Curlic).toString()
		let bref = new Curlic(entry.desc).toString()

		$retitle(entry.title)
		
		morph(this.content_node, html`
			<div>
				<nav>
					${raw(bref)}
					<hr />
					<pre>${raw(this.tree)}</pre>
				</nav>
				<main>
					${raw(self)}
				</main>
			</div>
		`)		
	}
}


figlet.preloadFonts(["Straight"], start);

function start() {
	Ø.tree = constructTree()
	Ø.start()
}

//

function constructTree() {
	let sorted = _.sortBy(_.map(Ø.database.lexicon, (v,k) => {
		v.key = k
		return v
	}), (o) => _.snakeCase(o.name))

	let things = _.groupBy(sorted, 'under')
	
	let str = ''

	for (cat of Object.keys(things)) {
		let pre = '<span class="accent">├╴</span>'
		let end = '<span class="accent">└╴</span>'
		if (cat == 'root') { pre=''; end='' }
		if (cat != 'root') str += '<span class="accent">' + cat + '/</span>' + '\n';
		for (th in things[cat]) {
			let self = things[cat][th]
			if (self.key == '404') continue;
			if (th < things[cat].length-1) {
				if (self.external) {
					str += pre + new Curlic('{' + self.key + '('+self.link+')}').toString() + '\n'
				} else {
					str += pre + new Curlic('{' + self.key + '('+$slugify(self.key)+')}').toString() + '\n'
				}
			} else {
				if (self.external) {
					str += end + new Curlic('{' + self.key + '('+self.link+')}').toString() + '\n'
				} else {
					str += end + new Curlic('{' + self.key + '('+$slugify(self.key)+')}').toString() + '\n'
				}
			}
		}
	}

	return str
}