function Indental(data)
{
  this.data = data;

  this.parse = function(type)
  {
    var lines = this.data.split("\n").map(liner)
    
    // Assoc lines
    var stack = {}
    var target = lines[0]
    for(id in lines){
      var line = lines[id]
      if(line.skip){ continue; }
      target = stack[line.indent-1];
      if(target){ target.children[target.children.length] = line }
      stack[line.indent] = line
    }

    // Format
    var h = {}
    for(id in lines){
      var line = lines[id];
      if(line.skip || line.indent > 0){ continue; }
      var key = _.snakeCase(line.content)
      h[key] = type ? new type(key,format(line)) : format(line)
    }
    return h
  }

  function format(line)
  {
    var a = [];
    var h = {};
    for(id in line.children){
      var child = line.children[id];
      if(child.key){ h[_.snakeCase(child.key)] = child.value }
      else if(child.children.length == 0 && child.content){ a[a.length] = child.content }
      else{ h[_.snakeCase(child.content)] = format(child) }
    }
    return a.length > 0 ? a : h
  }

  function liner(line)
  {
    return {
      indent:line.search(/\S|$/),
      content:line.trim(),
      skip:line == "" || line.substr(0,1) == "~",
      key:line.indexOf(" : ") > -1 ? _.snakeCase(line.split(" : ")[0].trim()) : null,
      value:line.indexOf(" : ") > -1 ? line.split(" : ")[1].trim() : null,
      children:[]
    }
  }
}