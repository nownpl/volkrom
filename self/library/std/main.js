/* DOM */
window.morph     = require('nanomorph')
window.html      = require('nanohtml')
window.raw       = require('nanohtml/raw')
window.Component = require('nanocomponent')

/* State */
window.bus   = require('nanobus')
window.state = require('nanostate')

/* Router */
window.router      = require('wayfarer')
window.router.walk = require('wayfarer/walk')

/* Other */
window.create_debug = require('debug')
window.query  = (href = window.location.href) => require('nanoquery')(href)
window.hash   = (hash = window.location.hash) => require('hash-match')(hash)

/* Utilities */
window._ = require('lodash')
window.S = require('s-js')
window.he = require('he')

window.figlet = require('figlet')
