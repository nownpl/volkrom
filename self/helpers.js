window.TITLE = '0110'
window.TITLE_TEMPLATE = _.template('<%= title %> → <%= section %>')

window.$slugify = (string) => {
	return _.snakeCase(_.deburr(string))
}

window.$escape = (string) => {
	return he.encode(string, {useNamedReferences: true})
}

window.$retitle = (string) => {
	if (!string) {
		document.title = TITLE
	} else {
		document.title = TITLE_TEMPLATE({title: TITLE, section: string})
	}
}