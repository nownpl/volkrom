localStorage.debug = 'volkrom,volkrom:*'

figlet.defaults({fontPath: "assets"});

const Ø = {
	bus: bus('volkrom'),
	trackers: {},
	content: {},
	database: {},
	node: document.getElementById('volkrom'),
	debug: create_debug('volkrom'),
	router: router('/404'),

	init() {
		this.content_node = html`<div></div>`
		this.node.appendChild(this.content_node)

		addEventListener('hashchange', () => { this.bus.emit('navigate') })

		this.router.on('/', () => {
			this.home(this.content.lexicon.HOME)
		})
		this.router.on('/:slug', ({ slug }) => {
			if ((typeof this.content.lexicon[_.toUpper(_.snakeCase(slug))]) !== 'undefined') {
				this.render(this.content.lexicon[_.toUpper(_.snakeCase(slug))])
			} else {
				this.debug(hash() + ' not found')
				this.render(this.content.lexicon['NOT_FOUND'])
			}
		})

		this.bus.on('navigate', () => {
			this.debug('navigate to '+hash())
			this.router(hash())
		})

		// get all trackers
		/*$.ajax({
			type: 'GET',
			url: 'https://wakatime.com/share/@496f0bb9-1f90-4ceb-afa7-40b8eabc7845/6feb11d3-4287-4df2-ac81-8c7c79dbf7ea.json',
			dataType: 'jsonp',
			success: (response) => {
				this.debug('retrieved WakaTime data')
				this.trackers.code = response.data
				this.bus.emit('trackers:code', response.data)
			},
		})*/

		this.content.lexicon = new Indental(DATABASE.lexicon).parse()
	},

	start() {
		this.debug('starting völkrom')
		this.init() 
		this.bus.emit('navigate')
	},

	render(entry) {
		let self = new Runic(entry.LONG, Curlic)
		let bref = new Curlic(entry.BREF)
		morph(this.content_node, html`<div><nav>${raw(bref)}</nav><main>${raw(self)}</main></div>`)
	},

	home(entry) {
		let self = new Runic(entry.LONG, Curlic)
		let bref = new Curlic(entry.BREF)		
		morph(this.content_node, html`<div><nav>${raw(bref)}</nav><main>${raw(self)}</main></div>`)
	}
} // volkröm global object

figlet.preloadFonts(["tp"], start);

function start() {
	Ø.start()
}